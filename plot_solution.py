from mpl_toolkits.mplot3d import Axes3D
import matplotlib 
matplotlib.use('Agg')
from matplotlib import pyplot as plt
from matplotlib import cm               # colormaps

import numpy as np
#import Tkinter
import sys
import cmath


k = -1
loading = True

global fig, ax, nl, kmax

nl = 0
kmax = 10000

fig = plt.figure()

while loading:

    k = k + 1

    stepnum = k
    stepname = str(stepnum)
    fname = "step" + stepname

    loaded = False

    try:
	infname = fname + ".txt"
	r, theta, u = np.loadtxt(infname, unpack=True)
	loaded = True
        nl = nl + 1
    except:
	if k == 0:
	    err_msg = "Could not load data from file %s." % infname \
              + " Did you forget to run the program?"
	    raise Exception(err_msg)
	else:
	    #print "%d steps were loaded" % nl
	    loaded = False
	
    if (loaded == False):
        if (k < kmax):
            continue
        else:
            print "%d steps were loaded" % nl
	    break


    #print u
    #print x

    n = r.size

    #ax.clf()
    ax = fig.add_subplot(1, 1, 1, projection='3d')


    #plt.axis([0,1,-1.2,1.2])
    #plt.plot(x, u, 'yo-')

    #fig = plt.figure()
    #ax = fig.add_subplot(1, 1, 1, projection='3d')
    #r = (45,60,10,29)
    #degree = (95*np.pi/180.0,180*np.pi/180.0,14,18)
    #theta = ((degree*np.pi/180.0),(degree*np.pi/180.0))
    #theta = degree
    #X = np.reshape(theta,(2,2))
    #Y = np.reshape(r,(2,2))
    #for all ri in r       X.append(r*cmath.sin(theta))
    X = np.zeros(n)
    Y = np.zeros(n)
    Z = u

    for i in range(0, n ):
      X[i] = r[i]*cmath.sin(theta[i])
      Y[i] = r[i]*cmath.cos(theta[i]) 

    ax.set_title('View', fontsize=20)
    ax.set_xlim(-1, 1)
    ax.set_ylim(-1, 1)
    ax.set_zlim(-1,1)
    X = np.concatenate([X,[0,0]])
    Y = np.concatenate([Y,[0,0]])
    Z = np.concatenate([Z,[1,-1]])
    #ax.plot_wireframe(X, Y, Z), 'yo-', rstride=4, cstride=4)
    ax.scatter(X, Y, Z, s=1,marker = ".", c="r")
    #ax.set_rmax(90.0)

    stepname = str(nl)
    fname = "t" + stepname
    pltfname = "plot_" + fname + ".png"
    plt.savefig(pltfname, dpi = 200)
    print 'Plot saved as %s' % pltfname

    fig.delaxes(ax)    

    #plt.show()

