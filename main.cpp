
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <math.h>


#include <sstream> // Required for stringstreams


std::string itos ( int number ){
  std::ostringstream oss;
  oss<< number;
  return oss.str();
} 

int save_vector(double* A, int N, const char* fname){
  using namespace std;
  ofstream out(fname,ios::out);

  string fname_str = fname;

  if(!out.fail()){
    for(int i = 0; i < N; i++) out << A[i] << endl;
    cout << "file " << fname_str << " was successfully created" << endl;
  }
  else{
    cout << "error creating output file" << endl;
  }
  
  out.close();
  
  return 0;
}

int save_grafic_2d_on01(double* A, int N, const char* fname){
  using namespace std;
  ofstream out(fname,ios::out);

  string fname_str = fname;

  if(!out.fail()){
    for(int i = 0; i < N; i++) out << (1.0*i)/(N - 1) <<" "<< A[i] << endl;
    cout << "file " << fname_str << " was successfully created" << endl;
  }
  else{
    cout << "error creating output file" << endl;
  }
  
  out.close();
  
  return 0;
}

int save_grafic_3d_onD(double* D, double* A, int N, const char* fname){
  using namespace std;
  ofstream out(fname,ios::out);

  string fname_str = fname;

  if(!out.fail()){
    for(int i = 0; i < N; i++) out << D[2*i] << " " << D[2*i + 1] << " " << A[i] << endl;
    cout << "file " << fname_str << " was successfully created" << endl;
  }
  else{
    cout << "error creating output file" << endl;
  }
  
  out.close();
  
  return 0;
}


int hyper_1d(){
  using namespace std;

  double *U0, *V0, *U1, *U2, *Ut;

  double T = 1.0;

  int M = 15;
  
  double h = 1.0/M;
  double tau = h/2;

  int N = T/tau;

  

  U0 = new double[M+1];
  U1 = new double[M+1];
  U2 = new double[M+1];
  V0 = new double[M+1];
  Ut = new double[M+1];

  cout<<"h = "<<h<<endl;
  cout<<"tau = "<<tau<<endl;

  // Initial conditions
  for(int i = 0; i <= M; i++) U0[i] = 0.0;//sin(M_PI/M*i);
  for(int i = 0; i <= M; i++){
    if (( i >= M/4) && (i <= 3*M/4)) 
      V0[i] = -2.0;
    else 
      V0[i] = 0.0;
  }

  U1[0] = 0.0;
  for(int i = 1; i < M; i++) U1[i] = U0[i];
  U1[M] = 0.0;

  U2[0] = 0.0;
  for(int i = 1; i < M; i++) U2[i] = U1[i] + tau*V0[i] + 1.0/2.0*(tau/h)*(tau/h)*(U1[i + 1] - 2*U1[i] + U1[i - 1]);
  U2[M] = 0.0;

  save_grafic_2d_on01(U1,M+1,"step0.txt");
  save_grafic_2d_on01(U2,M+1,"step1.txt");

  for(int k = 2; k < N; k++){

    for(int i = 1; i < M; i++) Ut[i] = 2*U2[i] - U1[i] + (tau/h)*(tau/h)*(U2[i + 1] - 2*U2[i] + U2[i - 1]);  

    for(int i = 1; i < M; i++) U1[i] = U2[i];

    for(int i = 1; i < M; i++) U2[i] = Ut[i];

    string fname_str = "step" + itos(k) + ".txt";

//    if( k == 7) save_vector(U2,M+1,"step7.txt");

    save_grafic_2d_on01(U2,M+1,fname_str.c_str());
    
  }

  save_grafic_2d_on01(U2,M+1,"stepN.txt");

  delete[](U0);
  delete[](U1);
  delete[](U2);
  delete[](V0);

  return 0;
}

int hyper_2d(){
  using namespace std;

  double *D, *U0, *V0, *U1, *U2, *Ut;

  double alpha = M_PI/4;
  double betta = M_PI/6;
  double R = 1.0;
  double epsilon = 0.05;

  double T = 1.0;

  int N = 200;
  int M = 100;
  
  double rho = (R - epsilon)/N;
  double ksi = (2*M_PI - 2*alpha)/M;

  double tau = rho/10;

  D = new double[2*(N+1)*(M+1)];
  U0 = new double[(N+1)*(M+1)];
  U1 = new double[(N+1)*(M+1)];
  U2 = new double[(N+1)*(M+1)];
  V0 = new double[(N+1)*(M+1)];
  Ut = new double[(N+1)*(M+1)];

  cout<<"rho = "<<rho<<endl;
  cout<<"ksi = "<<ksi<<endl;
  cout<<"tau = "<<tau<<endl;

  // Domain
  for(int i = 0; i <= N; i++){
    for(int j = 0; j <= M; j++){
      D[(i*(M+1) + j)*2] = i*rho + epsilon;	
      D[(i*(M+1) + j)*2 + 1] = j*ksi + alpha;	
    }
  }


  // Initial conditions
  for(int i = 0; i < (M + 1)*(N + 1); i++){
    U0[i] = 0.0;//sin(M_PI/M*i);
    U2[i] = 0.0;
  }

  save_grafic_3d_onD(D, U0, (M + 1)*(N + 1), "step0.txt");

  double i0 = (M_PI - betta - alpha)/ksi;
  double i1 = (M_PI + betta - alpha)/ksi;
  for(int i = 0; i < (M + 1)*(N + 1); i++){
    if ( (( i - (M + 1)*N) >= i0) && ((i - (M + 1)*N) <= i1) ) 
      U1[i] = 1.0;
    else if( (( i - (M + 1)*(N - 1)) >= i0) && ((i - (M + 1)*(N - 1)) <= i1) )
      U1[i] = 1.0;
    else 
      U1[i] = 0.0;
  }

  save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "step1.txt");

  for(int i = 0; i < (M + 1)*(N + 1); i++) U2[i] = 0.0;


  // Main loop
  double ri, rip1;
  int p = i0;
  int l = i1;
  int k = 2;
  for (double t = 2*tau; t < T; t += tau){

    //for(int i = 1; i < M; i++) Ut[i] = 2*U2[i] - U1[i] + (tau/h)*(tau/h)*(U2[i + 1] - 2*U2[i] + U2[i - 1]);  

    double rNm1 = epsilon + (N-1)*rho;
    double rN = epsilon + (N)*rho;

    for(int i = 1; i < N-1; i++){
      for(int j = 1; j < M-1; j++){
        ri = epsilon + i*rho;
        rip1 = ri + rho; 
        U2[i*(M+1) + j] = - U0[i*(M+1) + j] 
                          + U1[i*(M+1) + j]*(2 - (tau*tau/rho/rho) - (tau*tau*rip1)/(rho*rho*ri) - (2*tau*tau/ksi/ksi/ri/ri))
                          + U1[(i-1)*(M+1) + j]*(tau*tau/rho/rho)
                          + U1[(i+1)*(M+1) + j]*(tau*tau*rip1/rho/rho/ri)
                          + U1[(i)*(M+1) + j-1]*(tau*tau/ksi/ksi/ri/ri)
                          + U1[(i)*(M+1) + j+1]*(tau*tau/ksi/ksi/ri/ri);		
      }
    }
    
    
/*

    for(int i = N-1; i <= N-1; i++){
      for(int j = p; j < l; j++){
      
        double ksij = alpha + i*ksi;
         
        U2[i*(M+1) + j] =   2*U1[i*(M+1) + j]
                          - U0[i*(M+1) + j]
     - tau*tau/ksi/rNm1/rho*(   ksi*rNm1/rho( U0[i*(M+1) + j] - U0[(i-1)*(M+1) + j] )
                              + rho/ksi/rNm1( U0[i*(M+1) + j] - U0[(i)*(M+1) + j-1] )
                              - rho/ksi/rNm1*( U0[i*(M+1) + j+1] - U0[(i)*(M+1) + j] )
                              + ksi*rN/2/rho*( U0[i*(M+1) + j] - ksij );		
      }
    }
    
    
*/

    //cout << "inner cycle" << endl;


//    for(int i = 1; i < M; i++) U1[i] = U2[i];

//    for(int i = 1; i < M; i++) U2[i] = Ut[i];

    string fname_str = "step" + itos(k) + ".txt";

    if( 1 == 0 ){
      save_grafic_3d_onD(D, U2, (M + 1)*(N + 1), fname_str.c_str());
    }

    if( ( (int) (t/tau) ) % ( (int) (T/tau/100) ) == 0 ){
      cout << (int) (t/T*100 + 1) << "%" << endl;
      save_grafic_3d_onD(D, U2, (M + 1)*(N + 1), fname_str.c_str());
    }
    
    double* tempU = U0;
    U0 = U1;
    U1 = U2;
    U2 = tempU;

    k++;
  }

  save_grafic_3d_onD(D, U1, (M + 1)*(N + 1), "stepLast.txt");      

  delete[](D);
  delete[](U0);
  delete[](U1);
  delete[](U2);
  delete[](V0);

  return 0;

}


int main(){
  using namespace std;

  cout<<"Hello world world!"<<endl;

  hyper_2d();  

  return 0;
}
